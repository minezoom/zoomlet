FROM openjdk:8-jdk-alpine

COPY rootfs /

RUN apk update && \
    apk add screen && \
    apk add ca-certificates wget
CMD /init.sh