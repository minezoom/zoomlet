# Zoomlet

![Zoomlet Banner](http://i.imgur.com/DQL7qcP.png)


This docker image builds and runs an edition of Minecraft server software called [Paper](https://aquifermc.org/) with initial configuration files from [minezoom/server](https://gitlab.com/minezoom/server).

The server instance runs on a `screen` session on the Alpine Linux distribution.

# Requirements

* Docker 17.06 (Linux)

# Starting the container

```bash
docker run -i -t -p 25565:25565 registry.gitlab.com/minezoom/zoomlet
```

You will find the server session with `screen -r`.

Explanation:

* `-i -t` are used to allow for `Ctrl+C`/terminating the container and writing to the Minecraft console
* `-p 25565:25565` maps the container port `25565` to the host port `25565`

You might want to use `-v /host/path:/minecraft` to keep the Minecraft server files on your host machine at `/host/path`. In this case, unless you
are using root on your host machine, you may want to provide the `USER_ID` environment variable which is used to set the ownership of the `/minecraft`
files. Use `` -e USER_ID=`id -u` `` to provide your user id, or `` -e USER_ID=`id -u someone` `` for someone else's.
