#!/bin/ash

# the path to the Minecraft server binaries and configuration
DIR=/minecraft

# the start script which is run upon completing initialization
START=start.sh

# the server configuration (including the start script)
SERVER_ZIP=https://gitlab.com/minezoom/server/builds/artifacts/master/raw/target/server.zip?job=package
SERVER_TMP=/tmp/server.zip

# the user to create within the docker container, which will match the host container
USER_NAME=user

echo "Initializing zoomlet..."

if [ ! -z $USER_ID ]; then
  if ! id $USER_ID > /dev/null 2>&1; then
    echo "Creating user, $USER_NAME, with uid $USER_ID..."

    # -h: home directory
    # -u: uid
    # -D: no password
    adduser -D -h /minecraft -u $USER_ID $USER_NAME
  fi
else
  USER_NAME=root
fi

if [ ! -d $DIR ] || [ ! -f $DIR/$START ]; then
  echo "Server configuration not found, creating..."
  
  mkdir -p $DIR

  # clone server configuration as user
  su - $USER_NAME \
    -c "wget -O $SERVER_TMP $SERVER_ZIP && unzip $SERVER_TMP -d $DIR"

  # cleanup
  rm -rf $SERVER_TMP
fi

# ensure executable
chmod +x $DIR/$START

# start server as user
su - $USER_NAME -c "cd $DIR && screen -d -m -S server sh ./$START"

/bin/ash